import React from "react";

import "../styles/Footer.css";


function Footer() {
   return (
     <div className="footer">
       <div className="socialMedia">
         {/* <InstagramIcon /> <TwitterIcon /> <FacebookIcon /> <LinkedInIcon /> */}
       </div>
       <p> &copy; 2023 www.samsung.com</p>
       <div className="container row">
          <div className="col-3">
            <ul>
               <h6>Offer</h6>
              <li><a href="">Samsung Rewards</a></li>
              <li><a href="">Referral Program</a></li>
              <li><a href="">Education Offer Program</a></li>
              <li><a href="">Goverment Offer Program</a></li>
              <li><a href="">Militery Offer Program</a></li>
              <li><a href="">Empolyee Offer Program</a></li>
              <li><a href="">Galaxy Campus store</a></li>
            </ul>
          </div>
          <div className="col-3">
          <ul>
               <h6>Support</h6>
              <li><a href="">Chat with Us</a></li>
              <li><a href="">Product Help</a></li>
              <li><a href="">Order Helpe</a></li>
              <li><a href="">Your Account</a></li>
              <li><a href="">Register Your Product</a></li>
              <li><a href="">Contact Us</a></li>
              <li><a href="">Give Your Option</a></li>
            </ul>
          </div>
          <div className="col-3">
          <ul>
              <h6>location</h6>
              <li><a href="">Korea</a></li>
              <li><a href="">America</a></li>
              <li><a href="">Cambodia</a></li>
              <li><a href="">China</a></li>
              <li><a href="">Thailand</a></li>
              <li><a href="">Veitname</a></li>
              <li><a href="">Singapo</a></li>
              <li><a href="">London</a></li>
              <li><a href="">Indonesia</a></li>
            </ul>
          </div>
          <div className="col-3">
          <ul>
              <h6>shop</h6>
              <li><a href="">Phone</a></li>
              <li><a href="">Watch</a></li>
              <li><a href="">TV & Home Theater</a></li>
              <li><a href="">Memory & store</a></li>
              <li><a href="">Monitor</a></li>
              <li><a href="">Home Application</a></li>
              <li><a href="">Smart Home</a></li>
            </ul>
          </div>
          
        
       </div>
       <div className="end">

       </div>
     </div>
   );
 }
 
 export default Footer;