
import logo from '../images/logo.png'
import { Link } from 'react-router-dom';
import '../styles/MyNavBar.css'
import { useState } from 'react';



function MyNavBar() {
  const [openLinks, setOpenLinks] = useState(false);

  const toggleNavbar = () => {
    setOpenLinks(!openLinks);
  };
  return (
    <div className="navbar">
      <div className="leftSide" id={openLinks ? "open" : "close"}>
        <img src={logo} />
        <div className="hiddenLinks">
          <Link className='Link' to="/"> Home </Link>
          <Link className='Link' to="/menu"> Menu </Link>
          <Link className='Link' to="/about"> About </Link>
          <Link  className='Link'to="/contact"> Contact </Link>
        </div>
      </div>
      <div className="rightSide">
        <Link to="/"> Home </Link>
        <Link to="/menu"> Menu </Link>
        <Link to="/about"> About </Link>
        <Link to="/contact"> Contact </Link>
        <button onClick={toggleNavbar}>
          all
        </button>
      </div>
    </div>
  );
}

export default MyNavBar;