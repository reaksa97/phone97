
import './App.css';
import {Routes,Route} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import MyNavBar from './components/MyNavBar';
import MenuPage from './pages/MenuPage';
import HomePage from './pages/HomePage';
import Contact from './pages/Contact';
import AboutUs from './pages/AboutUs';
import Footer from './components/Footer';

function App() {
  return (
    <>
    <div className='nav'>
    <MyNavBar/>
    </div>
    <Routes>
        <Route path='/' index element={<HomePage/>}/>
        <Route path='/menu' index element={<MenuPage/>}/>
        <Route path='/contact' index element={<Contact/>}/>
        <Route path='/about' index element={<AboutUs/>}/>
    </Routes>
    <Footer/>
    </>
  );
}

export default App;
