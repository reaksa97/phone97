import React from 'react';
import { Link } from 'react-router-dom';
import '../styles/HomePage.css'
import sam from '../images/bg_sam.jpg';



function HomePage() {
  const myStyle={
    backgroundImage: `url(${sam})`,
    height:'100vh',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
};
  return (
    <div className="home" style={myStyle}>
      <div className="headcontainer">
      <h1>Samsung Company</h1>
      <p>Samsung, South Korean company <br /> 
      that is one of the world's largest producers of electronic devices</p>
      <Link to={"/menu"}>

        <button>menu</button>
      </Link>
      </div>
   </div >
  )
}

export default HomePage