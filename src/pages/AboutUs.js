import React from "react";
import  bg_sam from '../images/bg_sam.jpg'
import "../styles/AboutUs.css";
function AboutUs() {
  return (
    <div className="about">
      <div
        className="aboutTop"
        style={{ backgroundImage: `url(${bg_sam})` }}
      ></div>
      <div className="aboutBottom">
        <h1> ABOUT US</h1>
        <p>
        For a limited time only, on Samsung.com/Shop Samsung App, or purchase a new qualifying Galaxy device (“Qualifying Purchase”), send in your qualifying trade-in device to Samsung through the Samsung Trade-In Program, and if Samsung determines your trade-in device meets all eligibility requirements, you will receive a trade-in credit specific to your qualifying trade-in device to apply toward your Qualifying Purchase. Show More
48/28/23-9/11/23, or while supplies last, purchase a Tab S9 256GB, Tab S9+ 512GB, and/or Tab S9 Ultra 512GB ("Qualifying Purchase") for the price of the lower memory storage level at Samsung.com or the Shop Samsung app.Show More
        </p>
      </div>
    </div>
  );
}

export default AboutUs;